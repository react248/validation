const UniversalInput = (props) => {
  return <input type={props.inputType || "range"} placeholder="text"></input>;
};

export default UniversalInput;
